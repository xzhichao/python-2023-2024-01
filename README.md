# python-2023-2024-01

#### 介绍
* 人生苦短，我用Python！

![](https://miro.medium.com/v2/resize:fit:4800/format:webp/1*WizgUsFeUgISS7vkFl4dEA.jpeg)


#### 学科专业性质的描述

* Python 语言为业界流行的程序开发工具，Python因其明了简单、优雅、明确的可读特性及面向对象的优势，被广泛应用于网站系统开发、科学计算、数据分析、数据挖掘、系统编程、网页爬虫、文本处理、大数据、机器学习等等。本课程分为两部分，初阶包含语法基础及数据处理，中阶涵盖面向对象概念、网页及移动应用开发。课程安排由浅入深，并以实例和编程能力训练为主，使学生建立起基本的编程能力和编程思维。从基本语法训练开始，循序渐进，让学生喜欢用python 进行编程。学生上完本课程建立起编程基础及解决问题的通用技能。

#### 学生获得

> 1. 从Python 语言切入网站与移动应用开发，学习编程及程序开发。
> 2. 掌握Python 语言语法和用法。
> 3. 认识相关实践社群及编程库，从习得其更新最佳实践及最新技巧的途径。

#### 考核方式

1.  平时练习
2.  期中测试
3.  期末测试
4.  期末项目


#### 资源
1. [ZHICHAO-gitee](https://gitee.com/xzhichao/python-2023-2024-01)
2. [Head First Python - book](https://www.book123.info/detail/9787519813635#google_vignette)
3. [Datacamp - e-learning](https://www.datacamp.com/)
