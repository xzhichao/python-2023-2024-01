# Python - Week02 PYTHON基础知识
* 2023-09-11
* 课程：Python语言
* week02

## 本周内容

> 1. 安装Anaconda
> 2. Jupyter notebook 编译环境
> 3. 建立自己的仓库 —— 保持良好的学习记录
> 4. 了解模块的概念，尝试调用(import)
> 5. 学习使用1种模块，实现一些简单的功能
>> 1. time
>> 2. datetime
>> 3. help
> 6. <font style="color:red">尝试理解变量的概念，并尝试创建</font>
> 7. 三种基本的数据形态
>> 1. [数值（number）](https://docs.python.org/3/tutorial/introduction.html#numbers)
>> 2. [字符串(strings)](https://docs.python.org/3/tutorial/introduction.html#text)

 
## 参考学习资料

> 1. Head First Python P1-P46
> 2. [PYTHON之路---Day01](https://www.cnblogs.com/xuzhichao/p/11407192.html)
> 3. [PYTHON---教程](https://docs.python.org/3/tutorial/index.html)