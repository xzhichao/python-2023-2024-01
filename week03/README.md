# Python - Week03 PYTHON-编程思维基础构建
* 2023-09-18
* 课程：Python语言
* week03


## 本周内容

> 1. 了解模块的概念，尝试调用(import)
> 2. 学习使用1种模块，实现一些简单的功能
>> 1. time
>> 2. datetime
>> 3. help
> 3. <font style="color:red">变量、赋值、数据</font>
> 4. 三种基本的数据形态
>> 1. [数值（number）](https://docs.python.org/3/tutorial/introduction.html#numbers)
>> 2. [字符串(strings)](https://docs.python.org/3/tutorial/introduction.html#text)
> 5. 理解注释及使用注释
> 6. 理解if for range()
> 7. 练习与实践

## 参考学习资料

> 1. Head First Python P1-P46
> 2. [PYTHON之路---Day01](https://www.cnblogs.com/xuzhichao/p/11407192.html)
> 3. [PYTHON---教程](https://docs.python.org/3/tutorial/index.html)